Cheesecake::Application.routes.draw do

  devise_for :admins

  root to: "home#index"
  match "contact" => 'contacts#new'
  resources :news
  # other routes go here
  resources :comics do
    member do
      get ':slug' => 'comics#show'
    end
    resources :comic_pages

  end
  resources :illustrations
  resources :artists
  
  namespace :backend do
    root to: "comics#index"
    resources :artists
    resources :admins
    resources :news
    resources :illustrations
    resources :comics do
      resources :chapters do
        resources :comic_pages
      end
    end
  end

end
