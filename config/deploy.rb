# install rails3 with passenger gem
require 'bundler/capistrano'
# set :whenever_command, "bundle exec whenever"
# require "whenever/capistrano"

# cap deploy:setup
# cap deploy:cold
# cap deploy
set :rails_env, 'production'
set :domain, '178.21.118.216'
set :user, 'peterderuijter'

set :repository,  'git@github.com:ptrr/kaaskoek.git'
set :branch, "master"

set :scm, :git
set :deploy_to, "/data/sites/cheesecakestudios"
set :ssh_options, {:paranoid => true}

default_run_options[:pty] = true
default_run_options[:shell] = '/bin/bash --login' 
set :spinner, false
set :runner,'peterderuijter'
set :use_sudo, false

role :web, domain                        # Your HTTP server, Apache/etc
role :app, domain                        # This may be the same as your `Web` server
role :db,  domain, :primary => true      # This is where Rails migrations will run

namespace :mod_rails do
  desc "Restart Application"
  task :restart, :roles => :app do
    run "touch #{current_path}/tmp/restart.txt"
    #run "cd #{current_path} && bash restart.sh"
  end
end

namespace :deploy do
  desc "Link shared database.yml to shared"
  task :database_shared do
    unless File.exists?("#{shared_path}/database.yml")
      run "mv #{release_path}/config/database.yml #{shared_path}/database.yml"
    else
      run "rm #{release_path}/config/database.yml"
    end
    run "ln -nfs #{shared_path}/database.yml #{release_path}/config/database.yml"
  end
end


namespace :deploy do
  desc "Precompile assets for Rails 3.x pipeline"
  task :compile_assets do
    run "cd #{release_path} && bundle exec rake assets:precompile --trace RAILS_ENV=production"
  end
end
namespace :deploy do
  desc "Symlink shared configs and folders on each release."
  task :symlink_shared do
    run "ln -nfs #{shared_path}/uploads #{release_path}/public/uploads"
    #run "sudo chown -R www-data:www-data #{release_path}/public/"
  end
end
namespace :deploy do
  %w(start restart).each { |name| task name, :roles => :app do mod_rails.restart end }
end
namespace :bundle do
  desc "Bundle up"
  task :install do
    #run "echo before rm; ls ~/.rbenv; rm -rf ~/.rbenv; echo after rm; ls ~/.rbenv; true"
    run "rbenv version && cd #{latest_release} && bundle install \
  --quiet \
  --binstubs \
  --path vendor/gems \
  --without development test"
  end
end
namespace :deploy do
  desc "Create User"
  task :initialize_app do
    run "rake app:initialize -RAILS_ENV=production"
  end
end

namespace :deploy do

  namespace :db do

    desc <<-DESC
    Creates the database.yml configuration file in shared path.

      By default, this task uses a template unless a template \
      called database.yml.erb is found either is :template_dir \
      or /config/deploy folders. The default template matches \
      the template for config/database.yml file shipped with Rails.

      When this recipe is loaded, db:setup is automatically configured \
      to be invoked after deploy:setup. You can skip this task setting \
      the variable :skip_db_setup to true. This is especially useful \
      if you are using this recipe in combination with \
        capistrano-ext/multistaging to avoid multiple db:setup calls \
    when running deploy:setup for all stages one by one.
    DESC
    task :setup, :except => { :no_release => true } do

      default_template = <<-EOF
      base: &base
      adapter: mysql
      timeout: 5000
      development:
        database: #{shared_path}/db/development.sqlite3
        <<: *base
      test:
        database: #{shared_path}/db/test.sqlite3
        <<: *base
      production:
        database: #{shared_path}/db/production.sqlite3
        <<: *base
      EOF

      location = fetch(:template_dir, "config/deploy") + '/database.yml.erb'
      template = File.file?(location) ? File.read(location) : default_template

      config = ERB.new(template)

      run "mkdir -p #{shared_path}/db"
      run "mkdir -p #{shared_path}/config"
      put config.result(binding), "#{shared_path}/config/database.yml"
    end

    desc <<-DESC
    [internal] Updates the symlink for database.yml file to the just deployed release.
    DESC
    task :symlink, :except => { :no_release => true } do
      run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
    end

  end


  after "deploy:setup",           "deploy:db:setup"   unless fetch(:skip_db_setup, false)
  after "deploy:finalize_update", "deploy:db:symlink"

end
  namespace :symlink_the do
    task :public do
      run "ln -s /data/sites/cheesecakestudios/shared/public #{release_path}/public"
    end
  end


after 'deploy:update_code', "deploy:database_shared", 'deploy:symlink_shared', "deploy:compile_assets", "symlink_the:public"
