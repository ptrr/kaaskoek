# unicorn_rails -c /data/springest/current/config/unicorn.rb -E production -D

rails_env = ENV['RAILS_ENV'] || 'production'
app_path  = '/data/sites/cheesecakestudios/current'

# 4 workers and 1 master
worker_processes(rails_env == 'production' ? 4 : 2)

# Load rails+github.git into the master before forking workers
# for super-fast worker spawn times
preload_app true

# Restart any workers that haven't responded in 60 seconds
# 30 -> 60s because workers seem to time out during deploys
timeout 60

pid "/var/run/unicorn.pid"

# Listen on a Unix data socket
listen "/data/sites/cheesecakestudios/shared/sockets/unicorn.sock", :backlog => 64

stdout_path "#{app_path}/log/unicorn.log"
stderr_path "#{app_path}/log/unicorn_err.log"

# Set the working directory so Unicorn reloads from the correct directory
working_directory app_path

##
# REE
# http://www.rubyenterpriseedition.com/faq.html#adapt_apps_for_cow
if GC.respond_to?(:copy_on_write_friendly=)
  GC.copy_on_write_friendly = true
end

before_exec do |server|
  ENV["BUNDLE_GEMFILE"] = "/data/sites/cheesecakestudios/current/Gemfile"
end

before_fork do |server, worker|
  # Disconnect ActiveRecord, it will connect after the forking is done.
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.connection.disconnect!

  # The following is only recommended for memory/DB-constrained
  # installations.  It is not needed if your system can house
  # twice as many worker_processes as you have configured.
  #
  # # This allows a new master process to incrementally
  # # phase out the old master process with SIGTTOU to avoid a
  # # thundering herd (especially in the "preload_app false" case)
  # # when doing a transparent upgrade.  The last worker spawned
  # # will then kill off the old master process with a SIGQUIT.
  old_pid = "#{server.pid}.oldbin"
  if File.exists?(old_pid) && server.pid != old_pid
    begin
      sig = (worker.nr + 1) >= server.worker_processes ? :QUIT : :TTOU
      Process.kill(sig, File.read(old_pid).to_i)
    rescue Errno::ENOENT, Errno::ESRCH
      # someone else did our job for us
    end
  end

  # Throttle the master from forking too quickly by sleeping.  Due
  # to the implementation of standard Unix signal handlers, this
  # helps (but does not completely) prevent identical, repeated signals
  # from being lost when the receiving process is busy.
  sleep 1
end

after_fork do |server, worker|
  ##
  # Unicorn master loads the app then forks off workers - because of the way
  # Unix forking works, we need to make sure we aren't using any of the parent's
  # sockets, e.g. db connection
  defined?(ActiveRecord::Base) and
    ActiveRecord::Base.establish_connection

  # Reset the memcache-based object store
  Rails.cache.instance_variable_get(:@data).reset if Rails.cache.instance_variable_get(:@data).respond_to?(:reset)

  ##
  # Unicorn master is started as root, which is fine, but let's
  # drop the workers to springest:springest
  begin
    uid, gid = Process.euid, Process.egid
    user, group = 'www-data', 'www-data'
    target_uid = Etc.getpwnam(user).uid
    target_gid = Etc.getgrnam(group).gid
    worker.tmp.chown(target_uid, target_gid)
    if uid != target_uid || gid != target_gid
      Process.initgroups(user, target_gid)
      Process::GID.change_privilege(target_gid)
      Process::UID.change_privilege(target_uid)
    end
  rescue => e
    if rails_env == 'development'
      STDERR.puts "couldn't change user, oh well"
    else
      raise e
    end
  end
end
