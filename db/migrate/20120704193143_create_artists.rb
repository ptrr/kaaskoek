class CreateArtists < ActiveRecord::Migration
  def change
    create_table :artists do |t|
      t.string :firstname
      t.string :lastname
      t.string :suffix
      t.string :description
      t.string :slug

      t.timestamps
    end
  end
end
