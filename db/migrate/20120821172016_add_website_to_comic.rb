class AddWebsiteToComic < ActiveRecord::Migration
  def change
    add_column :comics, :website, :string
  end
end
