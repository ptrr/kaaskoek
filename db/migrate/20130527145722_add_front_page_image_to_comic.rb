class AddFrontPageImageToComic < ActiveRecord::Migration
  def self.up
    add_attachment :comics, :front_page
  end

  def self.down
    remove_attachment :comics, :front_page
  end
end
