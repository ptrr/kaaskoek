class AddImageToIllustration < ActiveRecord::Migration
  def self.up
    add_attachment :illustrations, :image
  end

  def self.down
    remove_attachment :illustrations, :image
  end
end
