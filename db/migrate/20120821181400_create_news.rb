class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.string :title
      t.text :body
      t.boolean :published
      t.references :artist

      t.timestamps
    end
  end
end
