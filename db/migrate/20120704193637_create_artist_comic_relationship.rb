class CreateArtistComicRelationship < ActiveRecord::Migration
  def up
  	create_table :artists_comics, :id => false do |t|
      t.references :artist
      t.references :comic

      t.timestamps
    end
    add_index :artists_comics, [:artist_id, :comic_id]
  end

  def down
  	drop_table :artists_comics
  end
end
