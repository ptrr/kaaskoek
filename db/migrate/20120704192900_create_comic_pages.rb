class CreateComicPages < ActiveRecord::Migration
  def change
    create_table :comic_pages do |t|
      t.integer :order
      t.string :comic_page_image
      t.references :chapter

      t.timestamps
    end
  end
end
