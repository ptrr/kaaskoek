class ChangeOrderColumn < ActiveRecord::Migration
  def up
    remove_column :comic_pages, :order
    add_column :comic_pages, :page_order, :integer, default: 1
  end

  def down
    remove_column :comic_pages, :page_order
    add_column :comic_pages, :order, :integer
  end
end
