class CreateIllustrations < ActiveRecord::Migration
  def change
    create_table :illustrations do |t|
      t.string :description
      t.string :image
      t.integer :artist_id

      t.timestamps
    end
  end
end
