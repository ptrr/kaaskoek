class AddComicImageToComicPages < ActiveRecord::Migration
  def self.up
    add_attachment :comic_pages, :comic_page_image
  end

  def self.down
    remove_attachment :comic_pages, :comic_page_image
  end
end
