class AddTitleToIllustration < ActiveRecord::Migration
  def change
  	add_column :illustrations, :title, :string
  end
end
