class CreateComicViews < ActiveRecord::Migration
  def change
    create_table :comic_views do |t|
      t.references :comic
      t.string :ip

      t.timestamps
    end
  end
end
