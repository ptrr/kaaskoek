# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130527161021) do

  create_table "admins", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admins", ["email"], :name => "index_admins_on_email", :unique => true
  add_index "admins", ["reset_password_token"], :name => "index_admins_on_reset_password_token", :unique => true

  create_table "artists", :force => true do |t|
    t.string   "firstname"
    t.string   "lastname"
    t.string   "suffix"
    t.string   "description"
    t.string   "slug"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.string   "nickname"
    t.string   "website"
  end

  create_table "artists_comics", :id => false, :force => true do |t|
    t.integer  "artist_id"
    t.integer  "comic_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "artists_comics", ["artist_id", "comic_id"], :name => "index_artists_comics_on_artist_id_and_comic_id"

  create_table "chapters", :force => true do |t|
    t.string   "title"
    t.integer  "comic_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "comic_pages", :force => true do |t|
    t.string   "comic_page_image"
    t.integer  "chapter_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.integer  "page_order",                    :default => 1
    t.string   "comic_page_image_file_name"
    t.string   "comic_page_image_content_type"
    t.integer  "comic_page_image_file_size"
    t.datetime "comic_page_image_updated_at"
  end

  create_table "comic_views", :force => true do |t|
    t.integer  "comic_id"
    t.string   "ip"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "comics", :force => true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "front_page"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.string   "slug"
    t.string   "website"
    t.string   "front_page_file_name"
    t.string   "front_page_content_type"
    t.integer  "front_page_file_size"
    t.datetime "front_page_updated_at"
  end

  add_index "comics", ["slug"], :name => "index_comics_on_slug", :unique => true

  create_table "illustrations", :force => true do |t|
    t.string   "description"
    t.string   "image"
    t.integer  "artist_id"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
    t.string   "title"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "news", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.boolean  "published"
    t.integer  "artist_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.string   "slug"
  end

  create_table "pages", :force => true do |t|
    t.string   "title"
    t.text     "body"
    t.boolean  "active"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
