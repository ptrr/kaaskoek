class News < ActiveRecord::Base
  attr_accessible :artist_id, :body, :published, :title
  extend FriendlyId
  friendly_id :title, use: :slugged
end