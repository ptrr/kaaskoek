class Page < ActiveRecord::Base
  attr_accessible :active, :body, :title
  validates_presence_of :title
  validates_uniqueness_of :title
end
