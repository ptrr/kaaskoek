class Illustration < ActiveRecord::Base
  attr_accessible :artist_id, :description, :image, :title, :artist
  belongs_to :artist
  validates_presence_of :artist_id, :image, :title
  attr_accessible :image
  has_attached_file :image, :styles => {  :thumb => "150x214>"  }, :default_url => "/images/:style/missing.png"
end
