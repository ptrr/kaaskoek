class ComicView < ActiveRecord::Base
  attr_accessible :comic_id, :ip
  validates_presence_of :ip, :comic_id
end
