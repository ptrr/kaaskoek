class ComicPage < ActiveRecord::Base
  attr_accessible :order, :comic_page_image, :chapter_id
  validates_presence_of :page_order, :comic_page_image
  belongs_to :chapter

  attr_accessible :comic_page_image
  has_attached_file :comic_page_image, :styles => { :home => "360x548>", :thumb => "150x214>" }, :default_url => "/images/:style/missing.png"
  def name
    "Page "+self.page_order.to_s
  end
end
