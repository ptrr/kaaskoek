class Artist < ActiveRecord::Base
  extend FriendlyId
  friendly_id :slug, use: :slugged
  attr_accessible :description, :firstname, :lastname, :slug, :suffix, :website
  has_and_belongs_to_many :comics
  has_many :news
  validates_presence_of :firstname, :lastname, :slug

  def name
    name = self.firstname + " "
    unless self.suffix.nil?
      name += self.suffix + " "
    end
    name += self.lastname
  end
end
