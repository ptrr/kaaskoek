class Chapter < ActiveRecord::Base
  attr_accessible :comic_id, :title
  belongs_to :comic
  validates_presence_of :comic_id
  has_many :comic_pages
end
