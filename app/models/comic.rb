class Comic < ActiveRecord::Base
  extend FriendlyId
  friendly_id :title, use: :slugged
  attr_accessible :description, :front_page, :title, :slug, :artists, :website, :artist_ids
  has_and_belongs_to_many :artists
  accepts_nested_attributes_for :artists
  validates_presence_of :title, :slug
  validates_uniqueness_of :title, :slug
  before_destroy :artist_connection_delete
  has_many :chapters

  attr_accessible :front_page
  has_attached_file :front_page, :styles => { :home => "360x548>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"

  #mount_uploader :front_page, FrontPageUploader
  def artist_connection_delete
    artists.clear
  end
end
