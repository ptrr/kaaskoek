class NewsController < ApplicationController
  layout "comics"
  def index
    @news = News.page(params[:page]).per(10)
  end
  def show
    @news = News.find(params[:id])
  end
end