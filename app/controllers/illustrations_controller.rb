class IllustrationsController < ApplicationController
  layout 'comics'
  def index
    @illustrations = Illustration.all
  end

  def show
    @illustrations = Illustration.find_by_id(params[:id])
  end
end