class ComicPagesController < ApplicationController
  layout 'gallery'

  def index
    @comic_pages = ComicPages.where(:chapter_id => params[:id])
  end
  
  def show
    @comic = Comic.where(slug: params[:comic_id]).first
    @comic_pages = @comic.chapters.map(&:comic_pages).flatten
    @chapters = @comic.chapters
    @comic_page = ComicPage.find_by_id(params[:id])
    # raise @comic_page.inspect
    if @comic_page.page_order.to_i.odd?
      @rightpage = ComicPage.where(:page_order => @comic_page.page_order+1, :chapter_id => @comic_page.chapter_id).first
      @leftpage = @comic_page
    else
      @leftpage = ComicPage.where(:page_order => @comic_page.page_order-1, :chapter_id => @comic_page.chapter_id).first
      @rightpage = @comic_page
    end
    unless @rightpage == ComicPage.where(chapter_id: @comic_page.chapter_id).last || @rightpage.nil?
      @nextpage = ComicPage.where(chapter_id: @comic_page.chapter_id, :page_order => @rightpage.page_order+1).first
    else
      @nextpage = nil
    end

    unless @leftpage == ComicPage.where(chapter_id: @comic_page.chapter_id).first || @leftpage.nil? 
      @previouspage = ComicPage.where(chapter_id: @comic_page.chapter_id, :page_order => @leftpage.page_order-1).first 
    else 
      @previouspage = nil
    end
  end
    
end