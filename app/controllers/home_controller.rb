class HomeController < ApplicationController
  layout 'application'
	def index
    @comics = Comic.limit(3)
    @news = News.limit(5)
    @artists = Artist.limit(5)
	end
end
