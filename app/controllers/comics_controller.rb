class ComicsController < ApplicationController
  layout 'comics'
	
  def show
		@comic = Comic.where(slug: params[:id]).first
	end

  def index
    @comics = Comic.all
  end
    
end