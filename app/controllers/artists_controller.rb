class ArtistsController < ApplicationController
  layout 'comics'
  def index
    @artists = Artist.all
  end

  def show
    @artist = Artist.find(params[:id])
  end
end