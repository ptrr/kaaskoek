class Backend::ArtistsController < Backend::ResourceController
	def create
		@artist = Artist.new(params[:artist])
		if @artist.save
			flash[:notice] = "Artist created!"
			redirect_to backend_artist_path(@artist)
		else
			flash[:alert] = "Something went wrong!"
			render :new
		end
	end
end