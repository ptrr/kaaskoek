class Backend::IllustrationsController < Backend::ResourceController
  def create
		@illustration = Illustration.new(params[:illustration])
		if @illustration.save
			flash[:notice] = "Illustration created!"
			redirect_to backend_illustrations_path
		else
			flash[:alert] = @illustration.errors.inspect
			render :new
		end
	end

	def update
		@illustration = Illustration.find(params[:id])

		if @illustration.update_attributes(params[:illustration])
			flash[:notice] = "Illustration updated!"
			redirect_to backend_illustrations_path
		else
			flash[:alert] = @illustration.errors.inspect
			render :new
		end
	end

end
