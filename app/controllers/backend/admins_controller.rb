class Backend::AdminsController < Backend::ResourceController
  def create
    @admin = Admin.new(params[:admin]) 
    if @admin.save
      redirect_to backend_admins_path
    else
      flash[:alert] = "Please fill in all the fields and make sure password and the confirmation are the same."
      render :new
    end
  end
end
