class Backend::ComicPagesController < Backend::ApplicationController
	def new
    @comic_page = ComicPage.new(chapter_id: params[:chapter_id])
    @chapter = Chapter.find(params[:chapter_id])
  end

  def create
    @comic_page = ComicPage.new(chapter_id: params[:chapter_id])
    @chapter = @comic_page.chapter
    @comic_page.comic_page_image = params[:comic_page][:comic_page_image]
    @comic_page.page_order = params[:comic_page][:page_order]
    if @comic_page.save
      flash[:notice] = "Page saved!"
      redirect_to backend_comic_chapter_path(:comic_id => @comic_page.chapter.comic.id, :id => @comic_page.chapter.id)
    else
      flash[:alert] = "Not saved!"
      render :new
    end
  end

  def edit
    @comic_page = ComicPage.find(params[:id])
    @chapter = Chapter.find(params[:chapter_id])
  end

  def update
    @comic_page = ComicPage.find(params[:id])
    @comic_page.comic_page_image = params[:comic_page][:comic_page_image]
    @comic_page.page_order = params[:comic_page][:page_order]
    if @comic_page.save
      flash[:notice] = "Page saved!"
      redirect_to backend_comic_chapter_path(:comic_id => @comic_page.chapter.comic.id, :id => @comic_page.chapter.id)
    else
      flash[:alert] = "Not saved!"
      render :new
    end
  end


end