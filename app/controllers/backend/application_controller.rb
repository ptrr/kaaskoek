class Backend::ApplicationController < ActionController::Base
  before_filter :authenticate_admin!
  protect_from_forgery
  layout 'backend'
end
