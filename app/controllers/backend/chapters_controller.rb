class Backend::ChaptersController < Backend::ResourceController
  def new
    @comic = Comic.where(:slug => params[:comic_id]).first
    @chapter = Chapter.new(comic_id: @comic.id)
  end

  def show
    @comic = Comic.find(params[:comic_id])
    @pages = ComicPage.where(chapter_id: params[:id]).order(:page_order)
    @chapter = Chapter.find(params[:id])
  end

  def edit
    @comic = Comic.where(:id => params[:comic_id]).first
    @chapter = Chapter.find(params[:id])
  end

  def create
    @chapter = Chapter.new
    @comic = Comic.where(:slug => params[:comic_id]).first
    @chapter.comic_id = @comic.id
    @chapter.title = params[:chapter][:title]
    if @chapter.save
      flash[:notice] = "Chapter saved successfully"
      redirect_to backend_comic_chapter_path(@chapter, comic_id: @comic.slug)
    else
      flash[:alert] = "Not saved!"
      render :new
    end
  end

  def update
    @chapter = Chapter.find(params[:id])
    @comic = Comic.where(:slug => params[:comic_id]).first
    @chapter.comic_id = @comic.id
    @chapter.title = params[:chapter][:title]
    if @chapter.save
      flash[:notice] = "Chapter saved successfully"
      redirect_to backend_comic_chapter_path(@chapter, comic_id: @comic.slug)
    else
      flash[:alert] = "Not saved!"
      render :edit
    end
  end
end
