class Backend::ComicsController < Backend::ResourceController
	def create	
		@comic = Comic.new
		@comic.artist_ids = Artist.where(id: params[:comic][:artists])
		@comic.title = params[:comic][:title]
		@comic.website = params[:comic][:website]
		@comic.description = params[:comic][:description]
		@comic.slug = params[:comic][:slug]
		@comic.front_page = params[:comic][:front_page]
		puts @comic.inspect
		if @comic.save
			flash[:notice] = "Comic created!"
			redirect_to backend_comic_path(@comic)
		else
			flash[:alert] = @comic.errors.inspect
			render :new
		end
	end

	def update
		@comic = Comic.find(params[:id])
    @comic.update_attributes(params[:comic])
		if @comic.save
			flash[:notice] = "Comic updated!"
			redirect_to backend_comic_path(@comic)
		else
			flash[:alert] = @comic.errors.inspect
			render :new
		end
	end
end
